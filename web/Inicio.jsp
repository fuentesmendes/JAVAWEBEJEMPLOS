<%-- 
    Document   : Inicio
    Created on : 9/05/2018, 04:21:22 PM
    Author     : dmendez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Pantalla de inicio Ejemplos</h1>

        <% String cedula = (String)session.getAttribute("cedu");  %>        
        <p>El numero de cedula es : <strong><%= cedula %></strong></p>

    </body>
</html>
