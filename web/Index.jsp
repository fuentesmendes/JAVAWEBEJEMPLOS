<%-- 
    Document   : Index
    Created on : 8/05/2018, 05:28:32 PM
    Author     : dmendez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="CSS/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="CSS/bootstrap.css" type="text/css">

    <title>Web Java Ejemplo</title>
  </head>
  <body>
      <div class="container">
          <div class="text-center">
              <h1><strong>Xynthesis</strong></h1>
          </div><br>
          <div>
              <%int edad = 32; %> <%-- Se declara una variable --%>
              <%-- <strong><%= edad %></strong>  Se muestra variable --%>
          </div>
          
          <div class="row">
              <div class="col-3"></div>
              <div class="col-6">
                  <form id="fmDatos" action="svtPersona" method="POST">
                      <div class="form-group row">
                          <%--<label for="ced" class="control-label col-3 d-flex justify-content-end"><strong>CEDULA :</strong></label>--%>
                          <input type="text" id="ced" name="txtCedula" placeholder="NUMERO DE CEDULA" class="form-control "/>
                      </div>
                      <div class="form-group row">
                          <%--<label for="nom" class="control-label col-3 d-flex justify-content-end"><strong>NOMBRES :</strong></label>--%>
                          <input type="text" id="nom" name="txtNombres" placeholder="NOMBRES" class="form-control"/>
                      </div>
                      <div class="form-group row">
                          <%--<label for="apell" class="control-label col-3 d-flex justify-content-end"><strong>APELLIDOS :</strong></label>--%>
                          <input type="text" id="apell" name="txtApellidos" placeholder="APELLIDOS" class="form-control "/>
                      </div>
                      <div class="form-group row">
                          <%-- <label for="ed" class="control-label col-3 d-flex justify-content-end"><strong>EDAD :</strong></label> --%>
                          <input type="number" id="ed" name="txtEdad" placeholder="EDAD" class="form-control "/>
                      </div>
                      <div class="form-group row">
                          <input type="submit" id="btnGuardar" value="GUARDAR" class="btn btn-primary btn-block"/>
                          <input type="button" id="btnCancelar" value="CANCELAR" class="btn btn-danger btn-block"/>
                      </div>
                  </form>
              </div>
              <div class="col-3"></div>
          </div>
          
          
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="JS/bootstrap.min.js" type="text/javascript"></script>
    <script src="boobootstrap.js" type="text/javascript"></script>
  </body>
</html>
