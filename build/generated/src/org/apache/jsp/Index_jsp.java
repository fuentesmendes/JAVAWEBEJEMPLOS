package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!doctype html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("  <head>\n");
      out.write("    <!-- Required meta tags -->\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("\n");
      out.write("    <!-- Bootstrap CSS -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/bootstrap.min.css\" type=\"text/css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/bootstrap.css\" type=\"text/css\">\n");
      out.write("\n");
      out.write("    <title>Web Java</title>\n");
      out.write("  </head>\n");
      out.write("  <body>\n");
      out.write("      <div class=\"container\">\n");
      out.write("          <div class=\"text-center\">\n");
      out.write("              <h1><strong>Xynthesis</strong></h1>\n");
      out.write("          </div><br>\n");
      out.write("          <div>\n");
      out.write("              ");
int edad = 32; 
      out.write(' ');
      out.write("\n");
      out.write("              ");
      out.write("\n");
      out.write("          </div>\n");
      out.write("          \n");
      out.write("          <div class=\"row\">\n");
      out.write("              <div class=\"col-3\"></div>\n");
      out.write("              <div class=\"col-6\">\n");
      out.write("                  <form id=\"fmDatos\" action=\"svtPersona\" method=\"POST\">\n");
      out.write("                      <div class=\"form-group row\">\n");
      out.write("                          ");
      out.write("\n");
      out.write("                          <input type=\"text\" id=\"ced\" name=\"txtCedula\" placeholder=\"NUMERO DE CEDULA\" class=\"form-control \"/>\n");
      out.write("                      </div>\n");
      out.write("                      <div class=\"form-group row\">\n");
      out.write("                          ");
      out.write("\n");
      out.write("                          <input type=\"text\" id=\"nom\" name=\"txtNombres\" placeholder=\"NOMBRES\" class=\"form-control\"/>\n");
      out.write("                      </div>\n");
      out.write("                      <div class=\"form-group row\">\n");
      out.write("                          ");
      out.write("\n");
      out.write("                          <input type=\"text\" id=\"apell\" name=\"txtApellidos\" placeholder=\"APELLIDOS\" class=\"form-control \"/>\n");
      out.write("                      </div>\n");
      out.write("                      <div class=\"form-group row\">\n");
      out.write("                          ");
      out.write("\n");
      out.write("                          <input type=\"number\" id=\"ed\" name=\"txtEdad\" placeholder=\"EDAD\" class=\"form-control \"/>\n");
      out.write("                      </div>\n");
      out.write("                      <div class=\"form-group row\">\n");
      out.write("                          <input type=\"submit\" id=\"btnGuardar\" value=\"GUARDAR\" class=\"btn btn-primary btn-block\"/>\n");
      out.write("                          <input type=\"button\" id=\"btnCancelar\" value=\"CANCELAR\" class=\"btn btn-danger btn-block\"/>\n");
      out.write("                      </div>\n");
      out.write("                  </form>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"col-3\"></div>\n");
      out.write("          </div>\n");
      out.write("          \n");
      out.write("          \n");
      out.write("      </div>\n");
      out.write("\n");
      out.write("    <!-- Optional JavaScript -->\n");
      out.write("    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>\n");
      out.write("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script>\n");
      out.write("    <script src=\"JS/bootstrap.min.js\" type=\"text/javascript\"></script>\n");
      out.write("    <script src=\"boobootstrap.js\" type=\"text/javascript\"></script>\n");
      out.write("  </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
